# SQL Server
## 概念
数据：表示信息的文字和符号

数据库管路系统DBMS：操纵和管理数据库的软件，用于创建、使用和维护数据库

数据库：用来储存数据的仓库

数据库系统：数据库系统主要由以上三部分组成

## 数据库的基本组成
**表**：关系

**行**：记录

**列**：也叫属性，字段

## 关系型数据库
关系型数据库：关系型数据库是依据关系模型来创建的数据库。关系型数据可以很好地存储一些关系模型的数据，比如一个老师对应多个学生的数据（“多对多”），一本书对应多个作者（“一对多”），一本书对应一个出版日期（“一对一”）
- Mysql(目前使用最多的一种数据库、功能性较弱)

- MariaDB

- SqlServer（最适合入门的数据库）

- PostgreSQL(功能最强大的开源数据库)

- Oracle (最成功的商业数据库，同时在走下坡路)

- Access(桌面数据库)

- Sqlite（桌面数据库）

- 达梦

- 金仓

- DB2

- Sybase

  
## 非关系型数据库
非关系型数据库：关系型数据库通过外键关联来建立表与表之间的关系，非关系型数据库通常指数据以对象的形式存储在数据库中，而对象之间的关系通过每个对象自身的属性来决定。
- MongoDb （文档型数据库，操作或者api最像关系型数据库的非关系型数据库）
- Redis （键值对数据库，内存数据库，速度极快，多用于高并发，秒杀，高缓存场景）
- Hbase （列存储型数据库，多用于大数据领域）
## 查询语句
### 基础语句
- 查询：select * from XXX
- 平均值：avg
- 最大值：max
- 最小值：min
- 记数：count
- 类似于、像：like
- 前几条记录：top
- 位于什么区间：between...and...
- 去重：distinct
- 总和：sum
- 投影查询：选择返回的数据集的字段的集合
- 多表查询：一次查询多个表
- 连接查询：通过join 关键来从多个表中获取信息
- 内连接：inner join
- 外连接：outer join、left join、right left
- 子查询：子查询常用```where```子句和```from```子句后边 select * from (select * form s1 where id = 1)
- 并集查询：union（查并集，自动消除重复行）union all : 不消除重复行
- 交集查询：intersect
- 差集查询：except
- 更新：update  XXX set yyy = a1，zzz= b2 where id= 1
- 插入：insert into XXX（） values（），（）
- 删除：delete XXX where id = x
- 分页查询：方法一：select * from StudentInfo order by stuid offset 查第m页:n*(m-1) rows fetch next 每页n条 rows only
- 方法二：n:每页几条， m:第几页
  -- select top N  * from 表名 where 主键 not in (select top n*(M-1)  主键 from 表名)