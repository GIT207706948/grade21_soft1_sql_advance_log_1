# 数据库

### 一、基本概念

1.数据Data： 一切表达(有意义的)信息的文字或者符号

2.数据库管理系统： 介于用户和操作系统之间的一个软件

3.数据库： 存储数据的仓库就叫数据库

4.数据库系统： 由数据库,数据库管理系统和数据库管理员一起组成的用于数据的存储，管理，维护等的 一整套系统



### 二、基本元素

1.数据库Database

2.数据表Table： 关系

3.数据行Row : 记录

4.数据列： 属性，字段



### 三、数据库的分类

#### 1、关系型数据库

-Mysql：目前使用最多的一种数据库、功能性较弱

-MairaDb

-SqlServer: 最适合入门的数据库

-Oracle： 最成功的商业数据库，同时在走下坡路

-PostgreSQL: 功能最强大的数据库开源数据库

-Access : 桌面数据库

-Sqlite ： 桌面数据库

-达梦： 国产数据库

-金仓： 国产数据库

-DB2

-Sybase

#### 2、非关系型数据库NoSql

-MongoDb: 文档型数据库，操作或者api最像关系型数据库的非关系型数据库

-Radis: 键值对数据库，内存数据库，速度极快，多用于高并发，秒杀，高缓存场景

-Hbase: 存储数据，多用于大数据领域



### 四、结构化查询语言SQL

#### 1、基本语句

##### -数据结构操作

###### 1.操作数据库 

1.创建数据库： create database 数据库名

2.删除数据库： drop database 数据库名 

3.使用/切换数据库： use 数据库名

4.修改数据库：

 (1)ALTER DATABASE Test1 MODIEF NAME=Test

 (2)exec sp_renamedb@dbname='Test_1',@newname='Test';

###### 2.操作数据库表

1.创建表： create table 表名 (字段名 数据类型 字段名 数据类型)

 2.删除表： drop table 表名 

3.修改表：

 (1)after table add column 字段名 数据类型

 (2)after table alter column 字段名 数据类型

 (3)after table delete column 字段名

##### --数据操作

-新增： insert into 表名(字段名，字段名) values (值1，值2)，（值1，值2）

 -删除 : delete 表名 where id=1 

-修改/更新 -查询

#### 2、常用方法

-最大值： max(字段名) 

-最小值： min(字段名)

 -平均值： avg(字段名)

 -总和： sum(字段名) 

-计数： count(*) 

-大小写转换： UPPER/LOWER

#### 3、常用句子

-排序

 -条件

 -分组

#### 4、查询形式

-投影查询: select 字段1，字段2....from 数据表名 

-多表查询: select a.*.b from a,b{where a.id=b.id} 

-连接查询 ：

1.内连接: inner join 

2.外连接： outer join:

 (1)left join:左连接

 (2)right outer join:右连接

 (3)full outer join:全连接

 -子查询 select * from(select * from 数据表) 

-union： 合并若干个查询结果，要求字段数量一致，并且数据类型兼容，会去除重复数据 

-union all： 合并若干个查询结果，要求字段数量一致，并且数据类型兼容，不会去除重复数据

 -分页查询： select * from 表名 order by 字段 offset跳过的行的数量 rows fetch next 一页包ss含的行的数量rows only