## 范式
### 范式的作用
#### 数据冗余：一样的数据一直出现，多余的数据。

#### 插入异常：如果由于某些原因主键暂时还没有或者还没全，其他数据也不能插入。

#### 删除异常：如果一些记录删了其他的与之无关的记录也会被跟着删掉。

#### 更新异常：一个数据进行了修改需要将所有与该数据有关的元组的该数据全改了，容易出现错误。
### 第一范式（1NF——不可再分割 ）
#### 定义：数据库表的每一列都是不可分割的原子数据项，而不能是集合，数组，记录等非原子数据项。如果实体中的某个属性有多个值时，必须拆分为不同的属性。
### 第二范式 （2NF——无部分依赖 ）

#### 定义：满足第一范式前提，且每一个非主属性完全函数依赖于码

只有当一个表中，主码由两个或以上的属性组成的时候，才会出现不符合第二范式的情况。

满足第一范式。即满足列的原子性，不可再分。

表中的每一个非主属性，必须完全依赖于本表码。

即没有包含在主键中的列或者说属性必须完全依赖于主键，而不能只依赖于主键的一部分。
### 第三范式（3Nf——无传递依赖）

#### 定义：在满足第二范式的基础上，且每一个非主属性既不部分依赖于码也不传递依赖于码。

满足第二范式

不能出现，非主键列 A 依赖于非主键列 B，非主键列 B 依赖于主键的情况。