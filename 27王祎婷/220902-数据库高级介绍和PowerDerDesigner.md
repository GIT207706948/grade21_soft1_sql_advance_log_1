# 数据库高级介绍和PowerDesigner安装

## 五.高级部分

	1. 数据库的设计
		A.数据库设计的规范
			（1）范式
				a.范式一
				b.范式二
				c.范式三
		B.数据库设计的示意图：ER图
		C.数据库设计的工具：PowerDesigner
	2.存储过程
	3.自定义函数
		A.表值函数
		B.标量函数
	4.触发器
	5.视图
	6.索引
	7.事务
	8.公用表表达式
	9.游标
	10.锁
	11.排名函数

## PowerDesigner的安装

### 下载地址：

1. 安装文件 链接: https://pan.baidu.com/s/1jIIgeZ8 密码: spk4
2. 破解文件 链接: https://pan.baidu.com/s/1jIIgeZO 密码: 24xv
3. 汉化文件 链接: https://pan.baidu.com/s/1pLA4siv 密码: eaji

### 安装过程：

1. 解压PowerDesigner安装文件，并双击PowerDesigner.exe；
2. 欢迎来到PowerDesigner安装界面，点击 Next
3. 一定要选择Trial，再点击Next，不要选择其它，这一步如果选择错，后面破解是不行的。
4. 选择Hong Kong，点击I AGREE，再点击Next；
5. 选择一个安装目录，默认是安装在C盘,也可以选择其他盘。再点击Next；
6. 点击General和Notaion，全选。再点击Next；
### 注意：走到这一 步，如果你电脑上已经安装了“Eclipse”软件，请不要打勾此插件选项。
7. 直接点击Next；进入正在安装界面
8. 点击Finish（安装完成）



