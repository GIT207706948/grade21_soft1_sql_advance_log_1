# makedown语法介绍
**Markdown是一种纯文本格式的标记语言。通过简单的标记语法，它可以使普通文本内容具有一定的格式。
建议使用typory软件进行编辑，typory是地表最强makedown编译器，没有之一，全体起立**

网页版markdown编译器：[语雀](https://www.yuque.com)

使用vscode建议下载markdown插件






---
阿里云不限速：「typora免费版」https://www.aliyundrive.com/s/xdBVkuTpifb 提取码: o34w



## 优点
1. 因为纯文本，所以不管在哪都可以使用makedown语法
2. 操作简单

## 缺点
1. **没有缺点**

## 基本格式

### 标题
一级标题 # 
二级标题 ##
三级标题 ###
....

```makedown
# 一级标题
## 二级标题
### 三级标题
```

### 字体

- 加粗

用两个*将要加粗的文字包裹起来

- 斜体

用一个*将要加粗的文字包裹起来

- 斜体加粗


用三个*将要加粗的文字包裹起来

- 删除线

要加删除线的文字左右分别用两个~~号包起来

实例：

```makedown
**这是加粗的文字**
*这是倾斜的文字*
***这是加粗倾斜的文字***
~~这是加删除线的文字~~
```

### 分割线

使用 --- 或 *** 都可以当做分割线

```
---

***

``` 

### 图片

语法:

```
![图片alt](图片地址)

```
示例：

```
![网络异常](https://niannyyu/test.png)
```
### 超链接

```

[超链接名](超链接地址)

```

```
[简书](http://jianshu.com)
```

### 列表
#### 无序列表
可以用 - + * 任何一种都可以

```
- 列表1
- 列表2
- 列表3
```
#### 有序列表
数字加点

```
1. 列表
2. 列表
3. 列表

```
### 代码块
```
语法：
用 ```将代码块包裹
实例：

```javascript

function fun(){
    echo"这是一句非常牛逼的代码";
}
fun();

```
