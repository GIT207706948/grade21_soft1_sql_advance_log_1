1.解压PowerDesigner安装文件，并双击PowerDesigner.exe;



2.在PowerDesigner安装界面，点击 Next；

![](https://img-blog.csdnimg.cn/3f9d8531784f40518b2e77705628e038.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAbXJ5Ng==,size_20,color_FFFFFF,t_70,g_se,x_16)

3.选择Trial，再点击Next，不要选择其它，这一步如果选择错，后面破解是不行的；

![](https://img-blog.csdnimg.cn/3ba7fc99c11d42468192546c135297d6.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAbXJ5Ng==,size_20,color_FFFFFF,t_70,g_se,x_16)

4.选择Hong Kong，点击I AGREE，再点击Next；

![](https://img-blog.csdnimg.cn/88d9364db27a40a3bd9d41b8da744259.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAbXJ5Ng==,size_20,color_FFFFFF,t_70,g_se,x_16)

5.选择一个安装目录，默认是安装在C盘。再点击Next；

![](https://img-blog.csdnimg.cn/f892cb9136a740379ba35318325ad2c3.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAbXJ5Ng==,size_20,color_FFFFFF,t_70,g_se,x_16)

6.点击General和Notaion，再点击Next；

![](https://img-blog.csdnimg.cn/205a911effb945b294279a38aae69d8c.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAbXJ5Ng==,size_20,color_FFFFFF,t_70,g_se,x_16)

注意：
走到这一 步，如果你电脑上没有安装“Eclipse”软件，请不要打勾此插件选项。否则 就会出现，让你选择“Eclipse”软件的安装路径。

![](https://img-blog.csdnimg.cn/7a60409886f448a58195c4248b58c73c.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAbXJ5Ng==,size_20,color_FFFFFF,t_70,g_se,x_16)

![](https://img-blog.csdnimg.cn/15a12c5a1e984a75a602836e5a721ae3.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAbXJ5Ng==,size_20,color_FFFFFF,t_70,g_se,x_16)

7.直接点击Next；进入正在安装界面。

![](https://img-blog.csdnimg.cn/acb9b96fd66043d1b55991d19d9b5948.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAbXJ5Ng==,size_20,color_FFFFFF,t_70,g_se,x_16)

8.点击Finish（安装完成）

![](https://img-blog.csdnimg.cn/06db22f76a2643428d867a86c34eb489.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAbXJ5Ng==,size_20,color_FFFFFF,t_70,g_se,x_16)

PowerDesigner永久使用
1、将PowerDesigner文件解压，然后，你能看到一个“pdflm16.dll”文件；
2、将“pdflm16.dll”复制并覆盖到你软件安装的目录中，一定要是此软件的安装根目录；例如：我这里就直接复制到 E:\powerdesigner 即可！

PowerDesigner汉化步骤
1、将PowerDesigner汉化文件解压，然后你能看到很多文件；
2、Ctrl+A（全选）所有文件，复制并覆盖到你软件安装的目录中；
3、如果点击PdShell16.exe不能正常启动，请点击pdlegacyshell16.exe启动；



下载地址：

1、安装文件 链接: https://pan.baidu.com/s/1jIIgeZ8 密码: spk4

2、破解文件 链接: https://pan.baidu.com/s/1jIIgeZO 密码: 24xv

3、汉化文件 链接: https://pan.baidu.com/s/1pLA4siv 密码: eaji



文章管理系统还没做