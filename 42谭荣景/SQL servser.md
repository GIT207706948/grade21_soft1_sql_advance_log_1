## **创建数据库**

```text
create database  数据库名称
```

## **使用数据库**

use 数据库

## **创建表**

```text
create table  表名
(
字段1    数据类型   条件约束
字段2    数据类型   条件约束
字段3    数据类型   条件约束
...
)
```

## **删除数据库**

```text
drop database 数据库名称
```

# 数据约束

主键(PRIMARY KEY):    能够唯一标识该数据表， 注：一个表只能有一个主键，但是一个主键可以由多个列组成。

非空(NOT NULL):    

identity(1,1)  自增

唯一(UNIQUE):    要求该列唯一，允许为空，但只能出现一个空值。

默认(DEFAULT):  某列的默认值，如我们的男性同学较多，性别默认为男。

检查(CHECK):    某列取值范围限制、格式限制等，如有关年龄的约束。

外键(FOREIGN KEY  表名):    只是对关系的有效性进行判断，关系本身存在，合不合理由外键决定。不是说有关系就有外键。

## **数据类型**

字符类型：char(n)，varchar(n)，text，image
整型类型：int(4字节)，smallint(2字节)，tinyint(1字节)
浮点类型：float(8字节)，real(4字节)，decimal(精度28位)
货币类型：money(8字节)，smallmoney(4字节)
日期时间类型：date(年月日)，datetime(年月日时分秒毫秒)，smalldatetime（年月日时分秒）
浮点类型：decimal(a,b) a指定指定小数点左边和右边可以存储的十进制数字的最大个数，最大精度38。b指定小数点右边可以存储的十进制数字的最大个数。小数位数必须是从 0 到 a之间的值。默认小数位数是 0。

# 修改数据表

ALTER TABLE 表名
ADD 列名 数据类型

ALTER TABLE 表名
DROP COLUMN 列名

ALTER TABLE 表名
ALTER COLUMN 列名 数据类型

# 插入数据

INSERT  [INTO] <表名> [(列名)] VALUES <值>

INTO 关键字可以省略
列名和值都可以有多个，每项列名或值之间可以用逗号隔开。
列名可以省，但是这需要保证VALUES 里各项数据的顺序和数据表中列的顺序一致。 

如果指定了列名，对具有默认值的列和允许为空的列插入数据，就需要用到 DEFAULT 和 NULL 关键字 

```HTML
INSERT INTO StuInfo（StuID,StuName,StuSex）
VALUES (1,NULL,DEFAULT) 
```

### 单句插入多行数据

```HTML
INSERT INTO StuInfo (StuID,StuName,StuSex）
VALUES(1,’张三’,0),(2,’李四’,1),(3,’王五’,1),(4,’赵六’,0),(5,’钱七’,0)
```

# 更新数据

```html
UPDATE <表名> SET  <列名=更新值> 
[WHERE <更新条件>]
```

# 删除数据

```HTML
DELETE FROM  <表名>  [WHERE  <删除条件>] 
```

# 删除表

```
TRUNCATE TABLE <表名>
DELETE FROM Students
DROP TABLE 表名
TRUNCATE 和DELETE只删除数据， DROP则删除整个表（结构和数据）。
```

# 数据查询

```
select * from <表名>
```

## 1.查询前N部分数据

```
select top N [percent] from <表名>     top是关键字     percent是百分比
```

## 2.查询指定列

```
select 列名1，列名2，列名3，...  from  <表名>
```

## 3.指定别名

```
select 列名1 [as] 别名1，列名2 [as] 别名2，列名3 [as] 别名3，...  from  <表名>
```

# 排序

```
select * from  <表名> order by 字段1 [desc/asc]，字段2 [desc/asc]
order by 是关键词。
字段 表示需要排序的字段。
desc表示降序排列，asc表示升序排列
```

```
SELECT    <列名> [as] 别名  
FROM      <表名>  
[ORDER BY <排序的列名>[ASC或DESC]]
```

# 消除重复行

## distinct是关键词

```
select distinct 字段名 from  <表名> 
```

# 条件筛选

```
SELECT    <列名> [as] 别名 FROM  <表名>  
[where 条件]
where: 关键字
条件 ： 表示筛选的条件

=   等于
<>  不等于
>   大于
<   小于
>=   大于等于
<=  小于等于
between  在某个范围内
is null  为空
is not null  不为空
in  包含在内
not in 不包含在内
like 搜索某种模式
```

# 模糊查询

```
like     %     _     [ ]    ^   
```

```
 select *  from   <表名> where 字段 like '%要查找的词%'
```

```
1. 字段 表示在哪个字段里进行模糊查找。
2. like 是关键词。
3. '%要查找的词%' 表示查找对应列内容包含 查找的词 的记录。
```

```
1. '要查找的词%' :    表示这个词要在字符串的开始。
2. '%要查找的词' :    那么表示这个词要在字符串的结尾。
3. '%要查找的词%':  表示这个词在字符串的任意位置都可以。
```

```
 select *  from   <表名> where 字段 like '__词__'
一个下划线代表单独的一个字符
```

# 聚合函数

```
总记录数      select count(列名) from <表名>          如果指定列名，count(字段)会忽略空值。count(*)则会把空值也计算进去。
```

```
求和          select sum(列名) from <表名>         返回特定数值列的总和，也可以作为乘积的总和使用
```

```
求均值        select avg(列名) from <表名>         用来确定特定数值列的平均值，且列名需要作为参数给出。
```

```
最小值        select min(列名) from <表名>        一般用来找出最大的数值或者日期值，要求指定列名，应用于文本列时返回排序后最后一列
```

```
最小值        select max(列名) from <表名>        一般用来找出最小数值或者日期值，要求指定列名，应用于文本列时返回排序后最前一列
```

# 组合聚合函数

```
聚合函数的使用：
1、 select 语句的选择列表（子查询或外部查询）；
2、having 子句；
3、compute 或 compute by 子句中等；
注意： 在实际应用中，聚合函数常和分组函数group by结合使用，用来查询。
```

# 分组查询

```
select 字段1，聚合函数(字段2)  from  <表名>  group by 字段1 

1. 字段1 表示需要分组的字段。
2. 字段2 表示聚合函数需要的字段。
```

# 分组查询注意事项

```
在使用GROUPBY子句前，需要知道一些重要的规定。

GROUP BY子句可以包含任意数目的列，因而可以对分组进行嵌套，更细致地进行数据分组。

如果在GROUP BY 子句中嵌套了分组，数据将在最后指定的分组上进行汇总。换句话说，在建立分组时，指定的所有列都起计算(所以不能从个别的列取回数据)。

GROUP BY子句中列出的每一列都必须是检索列或有效的表达式 (但不能是聚集函数)。如果在SELECT中使用表达式，则必须在GROUP BY子句中指定相同的表达式。不能使用别名。

大多数SQL实现不允许GROUP BY列带有长度可变的数据类型(如文本或备注型字段)。

除聚集计算语句外，SELECT 语句中的每一列都必须在 GROUP BY子句中给出。

如果分组列中包含具有NULL值的行，则NULL将作为一个分组返回。如果列中有多行NULL值，它们将分为一组。
GROUP BY 子句必须出现在WHERE子句之后，ORDER BY子句之前。

```

# 分组后筛选

```
select 字段1，聚合函数(字段2)  from  <表名>  group by 字段1 having 条件 
```

#                                  总结格式

```
SELECT [top n percent]    <列名> [as] 别名  
FROM      <表名>  
[WHERE 条件]
[GROUP BY 字段]
[HAVING 条件]
[ORDER BY <排序的列名>[ASC或DESC]]
```

# 连接查询之内连接

```
连接查询 根据两个或多个表之间的关系，从这些表中查询数据。

目的：实现多表查询。


分类：内连接  外连接  全连接  交叉连接。


内连接：inner join  使用比较运算符 = > < >= <= <> 进行表间的比较，查询与条件相匹配的数据。

等值连接 

结果：相匹配的数据查询出来，显示匹配出来的结果，如果没有匹配上，就没有结果。

显示连接    表 inner join  表  on 条件  where 

隐式连接  select .... from  表,表 where 关联条件
```

# 连接查询之外连接

```
外连接

外连接分类：左外连接、右外连接 全外连接  简称为：左连接  右连接  全连接


左连接 left (outer) join   on   返回左表的所有行，右表中没有匹配上，对应的列就显示NULL

结果：左表：所有行   右表：行数与左表相同，没有匹配上，显示NULL

右连接 right (outer) join  on  与左连接相反，返回的右表的所有行，左表进行匹配，左表中没有匹配上的，对应的列显示NULL

--结果：右表：所有行  左表：行数与右表相同，没有匹配上的，显示NULL
```

# 连接查询之全连接交叉连接

```
连接：内连接、外连接（左连接、右连接、全连接）、交叉连接

全连接：full (outer) join  全外连接  返回左表和右表中所有行，当某一行在另一个表中没有匹配，另一个表中的列返回NULL

交叉连接 cross join   迪卡尔积

如果不带where子句时，返回被连接的两个表的迪卡尔积，返回的行数是两个表行数的乘积。

带where子句，等价于inner join 返回的是匹配的数据。
```

# 子查询

```
select a,b,c                     ---OK，仅仅能存放单行子查询，不能使多行子查询

from tab1                  ---OK 能够有子查询

where col in(em1,em2)       ---能够有子查询

        col between a1 and a2

        col > 222

        col > ()

group by …                 ---不能够有子查询

having ….                  ---能够有子查询

order by …                 ---不能够有子查询

```

## 单行子查询

```
 select ename,deptno,sal
        from emp
        where deptno=(select deptno from dept where loc='NEW YORK')；
```

## 多行子查询

```
SELECT ename,job,sal
        FROM EMP
        WHERE deptno in ( SELECT deptno FROM dept WHERE dname LIKE 'A%')；
```

## 多例子查询

```
    SELECT deptno,ename,job,sal
        FROM EMP
        WHERE (deptno,sal) IN (SELECT deptno,MAX(sal) FROM EMP GROUP BY deptno)；
```

## 内联视图子查询

```
(1)SELECT ename,job,sal,rownum
          FROM (SELECT ename,job,sal FROM EMP ORDER BY sal)；
       (2)SELECT ename,job,sal,rownum
          FROM ( SELECT ename,job,sal FROM EMP ORDER BY sal)
          WHERE rownum<=5；
```

# 集合运算

--并集(union)， 交集(intersect)， 差集(except)
--union: 查并集，自动消除重复行
--union all : 不消除重复行
---关键词：union,union all ,   intersect ,  except

### --union: 并集

```
select * from 表
where 条件
union all
select * from 表
where 条件
```

### 交集(intersect)

```
select * from 表
where 条件
intersect
select * from 表
where 条件
```

### 差集(except)

```
select * from 表
where 条件
except
select * from 表
where 条件
```

# 分页查询

```
--方法一 ：offset
--语法：select * from Studentinfo order by StuId  offset 第几页 rows fetch next 每页几条 rows only
--总结：
--select * from StudentInfo order by stuid offset 查第m页:n*(m-1) rows fetch next 每页n条 rows only

列子
--假设每页2条记录，那么请查询出第1页的数据：
12 34 56 78 
select * from StudentInfo
select * from StudentInfo order by stuid offset 0 rows fetch next 3 rows only
select * from StudentInfo order by stuid offset 2 rows fetch next 3 rows only
--假设每页2条记录，那么请查询出第3页的数据
select * from StudentInfo order by stuid offset 4 rows fetch next 3 rows only
--假设每页2条记录，那么请查询出第4页的数据
select * from StudentInfo order by stuid offset 6 rows fetch next 3 rows only

--每页4条 查第3页
select * from StudentInfo order by stuid offset 4*(3-1) rows fetch next 4 rows only
```

```
--方法二：select top N * from 表 where 表主键 not in (Select top M 表主键 from Student)
--总结：
--n:每页几条， m:第几页
-- select top N  * from 表名 where 主键 not in (select top n*(M-1)  主键 from 表名)


列子
--假设每页3 条， 查第一页
select top 3 * from StudentInfo 
--查第二页
select top 3 * from StudentInfo where stuid not in (select top 3 stuid from StudentInfo )


--查第五页
select top 3 * from StudentInfo where stuid not in (select top 12 stuid from StudentInfo )
```

